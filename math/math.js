/**
 * Adds two numbers together
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Sum of operands
 */
function add(a, b) {
    return a + b;
}

/**
 * Subtracts two numbers
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Difference of operands
 */
function subtract(a, b) {
    return a - b;
}

/**
 * Multiplies two numbers together
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Product of operands
 */
function multiply(a, b) {
    return a * b;
}

/**
 * Divides two numbers
 *
 * @param a {Number} - Numerator
 * @param b {Number} - Denominator
 *
 * @returns {Number} - Quotient of operands
 */
function divide(a, b) {
    return a / b;
}

/**
 * Calculates the remainder when dividing two numbers
 *
 * @param a {Number} - Numerator
 * @param b {Number} - Denominator
 *
 * @returns {Number} - Remainder after division of operands
 */
function mod(a, b) {
    return a % b;
}
