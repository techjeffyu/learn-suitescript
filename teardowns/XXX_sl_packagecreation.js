define(["N/search", "N/ui/serverWidget", "N/url", "N/record", "N/http", "./XXX_shipment", "N/log"],
    function (s, ui, url, rec, http, shipment, log) {

    /**
     * Renders a form that allows the user to provide data describing a Package record, then creates
     * a corresponding Package record from that data upon submission.
     *
     * @exports xxx/package-creation/sl
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        log.audit({title: "Request received..."});

        var eventMap = {};
        eventMap[http.Method.GET] = handleGet;
        eventMap[http.Method.POST] = handlePost;

        eventMap[context.request.method] ?
            eventMap[context.request.method](context) :
            handleError(context);
    }

    function handleGet(context) {
        log.audit({title: "Processing GET request..."});

        var shipParentId = context.request.parameters.parentId;
        log.debug({title: "shipParentId", details: shipParentId});

        context.response.writePage(renderForm(shipParentId));
    }

    function handlePost(context) {
        log.audit({title: "Processing POST request..."});
        var packageId = shipment.create(readData(context));

        context.response.write('Package Created: ' + packageId);
    }

    function handleError(context) {
        throw {
            name: "XXX_UNSUPPORTED_REQUEST_TYPE",
            details: "This Suitelet only suports GET and POST requests."
        };
    }

    function renderForm(parentId) {
        log.audit({title: "Rendering form..."});

        var form = ui.createForm({
            title: 'Shipment Package Form'
        });

        form.addFieldGroup({
            id: 'custpage_grp_info',
            label: 'Primary Info'
        });
        form.addFieldGroup({
            id: 'custpage_grp_dimensions',
            label: 'Dimensions'
        });
        form.addFieldGroup({
            id: 'custpage_grp_submit',
            label: 'Submit'
        });

        var headfld = form.addField({
            id: 'custpage_xxx_shipment_parent',
            type: ui.FieldType.TEXT,
            label: 'Parent Shipment ID'
        });
        headfld.updateDisplayType({
            displayType: ui.FieldDisplayType.INLINE
        });
        headfld.defaultValue = parentId;

        form.addField({
            id: 'custpage_xxx_pkgnumber',
            type: ui.FieldType.TEXT,
            label: 'Package Number',
            container: 'custpage_grp_info'
        });
        form.addField({
            id: 'custpage_xxx_piece_count',
            type: ui.FieldType.TEXT,
            label: 'Piece Count',
            container: 'custpage_grp_info'
        });
        form.addField({
            id: 'custpage_xxx_packagetype',
            type: ui.FieldType.SELECT,
            label: 'Package Type',
            container: 'custpage_grp_info',
            source: 'customlist_xxx_packagetype'
        });
        form.addField({
            id: 'custpage_xxx_inventoryunits',
            type: ui.FieldType.TEXT,
            label: 'Inventory Count',
            container: 'custpage_grp_info'
        });
        form.addField({
            id: 'custpage_xxx_nmfc_number_line',
            type: ui.FieldType.SELECT,
            label: 'NMFC',
            container: 'custpage_grp_info',
            source: "customlist_xxx_nmfc"
        });
        form.addField({
            id: 'custpage_xxx_hazmat',
            type: ui.FieldType.CHECKBOX,
            label: 'Hazmat',
            container: 'custpage_grp_info'
        });
        form.addField({
            id: 'custpage_xxx_weight',
            type: ui.FieldType.TEXT,
            label: 'Weight',
            container: 'custpage_grp_dimensions'
        });
        form.addField({
            id: 'custpage_xxx_length',
            type: ui.FieldType.TEXT,
            label: 'Length',
            container: 'custpage_grp_dimensions'
        });
        form.addField({
            id: 'custpage_xxx_width',
            type: ui.FieldType.TEXT,
            label: 'Width',
            container: 'custpage_grp_dimensions'
        });
        form.addField({
            id: 'custpage_xxx_height',
            type: ui.FieldType.TEXT,
            label: 'Height',
            container: 'custpage_grp_dimensions'
        });

        form.addSubmitButton({
            label: 'Submit',
            container: 'custpage_grp_submit'
        });

        return form;
    }

    function readData(context) {
        log.audit({title: "Reading data from parameters..."});
        return {
            pkgNum: context.request.parameters.custpage_xxx_pkgnumber,
            pieceCount: context.request.parameters.custpage_xxx_piece_count,
            pkgType: context.request.parameters.custpage_xxx_packagetype,
            invUnits: context.request.parameters.custpage_xxx_inventoryunits,
            nmfcId: context.request.parameters.custpage_xxx_nmfc_number_line,
            weight: context.request.parameters.custpage_xxx_weight,
            length: context.request.parameters.custpage_xxx_length,
            width: context.request.parameters.custpage_xxx_width,
            height: context.request.parameters.custpage_xxx_height,
            hazmat: context.request.parameters.custpage_xxx_hazmat,
            shipParentId: context.request.parameters.custpage_xxx_shipment_parent
        };
    }

    exports.onRequest = onRequest;
    return exports;
});
