define([], function () {

    /**
     * Module Description...
     *
     * @exports XXX
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType UserEventScript
     */
    var exports = {};

    /**
     * <code>beforeLoad</code> event handler
     * for details.
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being loaded
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     * @param context.form
     *        {form} The current UI form
     *
     * @return {void}
     *
     * @static
     * @function beforeLoad
     */
    function beforeLoad(context) {
        log.audit({
            title: "UE beforeLoad triggered",
            details: context.type
        });
    }

    /**
     * <code>beforeSubmit</code> event handle
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being submitted
     * @param context.oldRecord
     *        {record} The old record before it was modified
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     *
     * @return {void}
     *
     * @static
     * @function beforeSubmit
     */
    function beforeSubmit(context) {
        log.audit({
            title: "UE beforeSubmit triggered",
            details: context.type
        });
        throw "Cannot save record."
    }

    /**
     * <code>afterSubmit</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     *
     * @param context.newRecord
     *        {record} The new record being submitted
     * @param context.oldRecord
     *        {record} The old record before it was modified
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     *
     * @return {void}
     *
     * @static
     * @function afterSubmit
     */
    function afterSubmit(context) {
        log.audit({
            title: "UE afterSubmit triggered",
            details: context.type
        });
    }

    exports.beforeLoad = beforeLoad;
    exports.beforeSubmit = beforeSubmit;
    exports.afterSubmit = afterSubmit;
    return exports;
});
