/**
 * A simple "Hello, World!" example of a Client Script. Uses the `pageInit`
 * event to write a message to the console log.
 */

function pageInit(type) {
    console.log("Hello, World from a 1.0 Client Script!");
}
